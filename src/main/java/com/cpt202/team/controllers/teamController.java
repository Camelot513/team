package com.cpt202.team.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.ArrayList;
import com.cpt202.team.models.Team;
import com.cpt202.team.repositories.TeamRepo;
import com.cpt202.team.services.TeamService;
//Restfull API
//Spring Anotation
@Controller
@RequestMapping("/team")
public class teamController {
    //private List<Team> teams = new ArrayList<Team>();

    @Autowired
    private TeamService teamService;

    // localhost:8080/team/list
    @GetMapping("/list")
    //也可以在这里加一个@RequestMapping("/team"),这样下面的GetMapping等就不用加team
    public String getList(Model model){
        model.addAttribute("teamList", teamService.getTeamList());
        return "allTeams";
    }

    //
    @GetMapping("/add")
    public String addTeam(Model model){
        model.addAttribute("team", new Team());
        return "addTeam";
        //return teamService.newTeam(team);
        //teams.add(team);
    }

    @PostMapping("/add")
    public String confirnNewTeam(@ModelAttribute("team") Team team){
        //add the new team into database
        teamService.newTeam(team);
        return "home";
    }

    //JSON
    //{"name":"Jason",
    // "memCount":6}

}
